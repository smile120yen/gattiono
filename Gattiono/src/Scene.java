import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;

public class Scene {
	String name=new String("none");
	IOStatus io;
	public Scene(IOStatus io){
		this.io=io;
	}
	public Scene update(){
		return this;
	}
	public void draw(Graphics g){
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, DFN.width, DFN.height);
		g.setColor(Color.BLACK);
		g.drawString("SceneName:"+name+" mouseX:"+io.mouseX+" mouseY:"+io.mouseY, DFN.width/2, DFN.height/2);
	}
	public String getName(){
		return new String(name);
	}
}
