import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class Furniture {
	int x, y,xsize=0,ysize=0;
	int offsetX, offsetY;
	ArrayList<Furniture> fnts;
	ArrayList<WallObject> walls;
	Color color;
	double impact;

	public Furniture(ArrayList<Furniture> fnts, int x, int y) {
		this.x = x;
		this.y = y;
		this.fnts = fnts;
		walls = new ArrayList<WallObject>();
		color=new Color(255,0,0);
	}

	public void update() {
		impact += 0.1;
		if (impact > 0)
			impact = 0;
		if (impact < -20) {
			impact = -20;
		}
		impact += this.getImpact() * 5;
		
		Iterator<WallObject> itr = walls.iterator();
		while (itr.hasNext()) {
			itr.next().update();
		}
	}

	public void draw(Graphics g) {
		Iterator<WallObject> itrw = walls.iterator();
		while (itrw.hasNext()) {
			itrw.next().draw(g);
		}
	}

	public void setOffset(int offsetX, int offsetY) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		Iterator<WallObject> itrw = walls.iterator();
		while (itrw.hasNext()) {
			itrw.next().setOffset(offsetX, offsetY);
		}
	}

	public double getImpact() {
		double tmp = 0;
		Iterator<WallObject> itr = walls.iterator();
		while (itr.hasNext()) {
			tmp += itr.next().getImpact();
		}
		return tmp;
	}

	public boolean checkBreak() {
		return false;
	}

	public float getParthHP() {
		double minhp, maxhp;
		Iterator<WallObject> itr = walls.iterator();
		WallObject tmp = itr.next();
		minhp = tmp.hp;
		maxhp = tmp.mhp;
		while (itr.hasNext()) {
			tmp = itr.next();
			if (tmp.hp < minhp) {
				minhp = tmp.hp;
				maxhp = tmp.mhp;
			}
		}
		return (float)minhp / (float)maxhp;
	}
	public ArrayList<BreakedObject> destructor(ArgList lists,int minX,int minY,int maxX,int maxY){
		ArrayList<BreakedObject> newbrks=new ArrayList<BreakedObject>();
		for(int i=0; i<50; i++){
			newbrks.add(new Effect(lists, (int)(Math.random()*xsize+x), (int)(Math.random()*ysize+y), (int)(Math.random()*50)+30, minX,minY, maxX, maxY,color));
		}
		return newbrks;
	}
}
