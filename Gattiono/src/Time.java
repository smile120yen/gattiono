import java.awt.Graphics;

public class Time {
	double startTime, nowTime;
	double time;
	int min,sec,x,y;
	Images images;
	public Time(Images images){
		startTime = System.currentTimeMillis();
		this.images=images;
		images.loadImage("0");
		images.loadImage("1");
		images.loadImage("2");
		images.loadImage("3");
		images.loadImage("4");
		images.loadImage("5");
		images.loadImage("6");
		images.loadImage("7");
		images.loadImage("8");
		images.loadImage("9");
		images.loadImage("coron");
	}
	public void setDrawPoint(int x,int y){
		this.x=x;
		this.y=y;
	}
	public void update(){
		time = 5*100 * 60 * 10 - nowTime;
		min=(int) (time / 1000 / 60);
		sec=(int) ((time / 1000) % 60);
		nowTime = System.currentTimeMillis() - startTime;
	}
	public void draw(Graphics g){
		int offset=0;
		
		char[] minArray=new Integer(min).toString().toCharArray();
		for(int i=0; i<minArray.length; i++){
			g.drawImage(images.get(String.valueOf(minArray[i])), x+offset, y, null);
			offset+=images.get(String.valueOf(minArray[i])).getWidth();
		}
		
		g.drawImage(images.get("coron"), x+offset, y, null);
		offset+=images.get("coron").getWidth();
		
		char[] secArray=new Integer(sec).toString().toCharArray();
		for(int i=0; i<secArray.length; i++){
			g.drawImage(images.get(String.valueOf(secArray[i])), x+offset, y, null);
			offset+=images.get(String.valueOf(secArray[i])).getWidth();
		}
	}
}
