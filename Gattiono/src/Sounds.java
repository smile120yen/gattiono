import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class Sounds {
	HashMap<String,Sound> sounds;
	Sounds(){
		sounds=new HashMap<String,Sound>();
	}
	public void start(String name){
		if(sounds.containsKey(name)){
			sounds.get(name).start();
		}else{
			System.out.println("\""+name+"\" not load yet");
		}
	}
	public void load(String name,String filename){
		if(!sounds.containsKey(name)){
			sounds.put(name, new Sound(filename));
			sounds.get(name).setOnce();
		}
	}
}
