import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class TitleScene extends Scene {
	Images images;
	Stage currentStage;
	int size;
	boolean changeScene=false,meow=false;
	HashMap<String,Stage> stages;
	Score score;
	Sounds sounds;
	
	public TitleScene(IOStatus io) {
		super(io);
		name = new String("Title");

		images = new Images();
		images.loadImage("title");
		images.loadImage("back");
		images.loadImage("wall");
		images.loadImage("head");
		
		score=new Score(images);
		
		stages=new HashMap<String,Stage>();
		stages.put("Title", new TitleStage(images,stages,score,io));
		
		currentStage=stages.get("Title");
		size=(int)Math.sqrt(DFN.width*DFN.width+DFN.height*DFN.height)+500;
		
		sounds=new Sounds();
		sounds.load("bgm","nc10812.wav");
		sounds.start("bgm");
		sounds.load("meow","nc105400.wav");
	}

	public Scene update() {
		currentStage.update();
		changeScene=currentStage.changeScene;
		if(changeScene==true){
			if(!meow){
				sounds.start("meow");
				meow=true;
			}
			size-=2;
			if(size<0){
				return new GameScene(io);
			}
		}
		return this;
	}

	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, DFN.width, DFN.height);
		currentStage.draw(g);
		g.drawImage(images.get("title"), 0,0, null);
		g.drawImage(images.get("head"), 0, 30 - currentStage.offsetY / 10, null);
		drawEnd(g,size);
	}
	public void drawEnd(Graphics g,int size){
		
		Graphics2D g2 = (Graphics2D)g;
	    Rectangle2D e1 = new Rectangle2D.Double(0,0,DFN.width,DFN.height);
	    Ellipse2D e2 = new Ellipse2D.Double(DFN.width/2-size/2,DFN.height/2-size/2,size,size);
	    Area a1 = new Area(e1);
	    Area a2 = new Area(e2);
	    a1.subtract(a2);
	    g2.setColor(Color.BLACK);
	    g2.fill(a1);
	}
}
