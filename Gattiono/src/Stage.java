import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class Stage {
	ArrayList<Furniture> fnts;
	int offsetX, offsetY, minX, minY, maxX, maxY, width, height;
	String name;
	Hand hand;
	HashMap<String, Stage> stages;
	IOStatus io;
	ArrayList<BreakedObject> brks;
	HashMap<String, Button> buttons;
	ArrayList<BreakedObject> effects;
	Boolean changeScene = false;
	Score score;
	Images images;
	Sounds sounds;
	ArgList lists;

	public Stage(Images images, HashMap<String, Stage> stages, Score score, IOStatus io) {
		fnts = new ArrayList<Furniture>();
		brks = new ArrayList<BreakedObject>();
		effects = new ArrayList<BreakedObject>();
		buttons=new HashMap<String, Button>();
		sounds=new Sounds();
		this.stages = stages;
		hand = new Hand(this, images);
		this.io = io;
		this.score = score;
		this.images=images;
		lists=new ArgList(brks, buttons, effects, fnts,sounds);
	}

	public Stage update() {
		updateOffset();
		updateHand();
		updateObjects();
		return this;
	}

	public void updateHand() {
		hand.update(io);
	}

	public void updateObjects() {
		for (int i = fnts.size() - 1; i >= 0; i--) {
			Furniture tmp = fnts.get(i);
			tmp.update();
			if (tmp.checkBreak()) {
				tmp.destructor(lists, minX, minY, maxX, maxY);
				fnts.remove(i);
			}
		}
		for (int i = brks.size() - 1; i >= 0; i--) {
			brks.get(i).update();
			if (brks.get(i).hp < 0) {
				brks.get(i).destructor(brks, minX, minY, maxX, maxY);
				score.addScore(brks.get(i).addScore, (int) brks.get(i).x, (int) brks.get(i).y);
				brks.remove(i);
			}
		}
		Iterator<BreakedObject> eitr = effects.iterator();
		while (eitr.hasNext()) {
			BreakedObject tmp = eitr.next();
			tmp.update();
			if (tmp.hp < 0) {
				eitr.remove();
			}
		}
		score.update();
	}

	public void draw(Graphics g) {
		drawObjects(g);
		hand.draw(g);
	}

	public void drawObjects(Graphics g) {
		Iterator<Furniture> itr = fnts.iterator();
		while (itr.hasNext()) {
			itr.next().draw(g);
		}
		for(int i=0; i<brks.size(); i++){
			brks.get(i).draw(g);
		}
		Iterator<BreakedObject> eitr = effects.iterator();
		while (eitr.hasNext()) {
			eitr.next().draw(g);
		}
	}

	public void updateOffset() {
		if (io.keyStatus.get("up") == 1) {
			offsetY--;
			if (this.getMinY() - 50 > offsetY)
				offsetY = this.getMinY() - 50;
		}
		if (io.keyStatus.get("down") == 1) {
			offsetY++;
			if (this.getMaxY() + 150 - DFN.height < offsetY)
				offsetY = this.getMaxY() - DFN.height + 150;
		}
		if (io.keyStatus.get("left") == 1) {
			offsetX--;
			if (this.getMinX() - 50 > offsetX)
				offsetX = this.getMinX() - 50;
		}
		if (io.keyStatus.get("right") == 1) {
			offsetX++;
			if (this.getMaxX() + 50 - DFN.width < offsetX)
				offsetX = this.getMaxX() - DFN.width + 50;
		}
		hand.setOffset(offsetX, offsetY);
		Iterator<Furniture> itr = fnts.iterator();
		while (itr.hasNext()) {
			itr.next().setOffset(this.offsetX, this.offsetY);
		}
		Iterator<BreakedObject> bitr = brks.iterator();
		while (bitr.hasNext()) {
			bitr.next().setOffset(this.offsetX, this.offsetY);
		}
		score.setOffset(offsetX, offsetY);
	}

	public double getImpact() {
		double tmp = 0;
		Iterator<BreakedObject> itr = brks.iterator();
		while (itr.hasNext()) {
			tmp += itr.next().getImpact();
		}
		return tmp;
	}

	public int getMaxX() {
		return maxX;
	}

	public int getMaxY() {
		return maxY;
	}

	public int getMinX() {
		return minX;
	}

	public int getMinY() {
		return minY;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
