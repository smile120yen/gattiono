import java.util.ArrayList;

public class SimpleWall extends Furniture{

	public SimpleWall(ArrayList<Furniture> fnts,int x, int y,int x2,int y2) {
		super(fnts,x, y);
		// TODO Auto-generated constructor stub
		walls.add(new WallObject(x,y,x2,y2));
		walls.add(new WallObject(x2,y2,x,y));
	}
	
}