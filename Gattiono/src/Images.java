import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class Images {
	HashMap<String,BufferedImage> images;
	Images(){
		images=new HashMap<String,BufferedImage>();
	}
	public BufferedImage get(String name){
		if(images.containsKey(name)){
			return images.get(name);
		}else{
			System.out.println("\""+name+"\" not load yet");
			return images.get("none");
		}
	}
	public void loadImage(String name){
		if(!images.containsKey(name)){
		BufferedImage readImage = null;
	    try {
	      readImage = ImageIO.read(new File("image/"+name+".png"));
	    } catch (Exception e) {
	      e.printStackTrace();
	      readImage = null;
	    }
		images.put(name,readImage);
		}
	}
}
