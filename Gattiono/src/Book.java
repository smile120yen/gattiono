import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

public class Book extends BreakedObject{
	Images images;
	int xsize,ysize;
	Color color;
	public Book(ArgList lists, float x, float y,  int stageMinX,
			int stageMinY, int stageMaxX, int stageMaxY,Images images) {
		super(lists, x, y, 0, stageMinX, stageMinY, stageMaxX, stageMaxY);
		// TODO Auto-generated constructor stub
		this.images=images;
		images.loadImage("book");
		this.ysize=images.get("book").getHeight();
		this.xsize=images.get("book").getWidth();
		this.size=xsize;
		color=new Color(0,94,0);
		//addScore=100;
		this.lists.sounds.load("impact","soccer-ball1.wav");
	}
	public void draw(Graphics g) {
		double drawX = x - offsetX;
		double drawY = y - offsetY;
		Graphics2D g2 = (Graphics2D) g;
		if (status == stop) {
			AffineTransform at = new AffineTransform();
			at.setToRotation(Math.toRadians(rad), drawX, drawY);
			g2.setTransform(at);
			g2.drawImage(images.get("book"),(int) (drawX - xsize / 2), (int) (drawY - ysize / 2),null);
			at = new AffineTransform();
			g2.setTransform(at);
		} else if (status == hop) {
			AffineTransform at = new AffineTransform();
			at.setToRotation(Math.toRadians(rad), drawX, drawY);
			g2.setTransform(at);
			//g2.fillRect((int) (drawX - size / 2), (int) (drawY - size / 2), (int) size, (int) size);
			g2.drawImage(images.get("book"),(int) (drawX - xsize / 2), (int) (drawY - ysize / 2),null);
			//g2.setColor(new Color(255,0,0,(int)(0.8*255*(1-(float)hp/(float)mhp))));
			//g2.fillRect((int) (drawX - xsize / 2), (int) (drawY - ysize / 2), xsize, ysize);
			at = new AffineTransform();
			g2.setTransform(at);
		}
	}
	public void updateAfter(){
		if(impact<-0.5){
			lists.sounds.start("impact");
		}
	}
	public void destructor(ArrayList<BreakedObject> brks,int minX,int minY,int maxX,int maxY){
		for(int i=0; i<2; i++){
			brks.add(new Effect(lists, (int)(Math.random()*xsize+x), (int)(Math.random()*ysize+y), xsize, minX,minY, maxX, maxY,color));
		}
	}
	
}
