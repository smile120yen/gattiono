import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javazoom.jl.decoder.JavaLayerException;

public class BreakedObject {
	// m:重量 res:反発率
	public double x, y, size, xsize,ysize,vx, vy, ax, ay, rad, m = 1, res, hp,mhp, impact,amp=0.9;
	public int addScore=0;
	protected int minX, maxX, minY, maxY, offsetX, offsetY, oldX, oldY, stopTime;
	private double G = 0.001;
	int status;
	static final int stop = 0, hop = 1;
	double oldHP;
	boolean Gravity = true;
	ArrayList<BreakedObject> brks;
	ArrayList<Furniture> fnts;
	ArgList lists;

	public BreakedObject(ArgList lists,float x, float y, float size,
			int stageMinX, int stageMinY, int stageMaxX, int stageMaxY) {
		/*
		 * statusについて stop：静止している状態。全てのオブジェクトはこの状態からスタート hop:跳ねまわっている状態
		 */
		this.lists=lists;
		status = stop;
		this.brks = lists.brks;
		this.fnts = lists.fnts;
		brks.add(this);
		this.x = x;
		this.y = y;
		this.vx = 0;
		this.vy = 0;
		this.size = size;
		this.xsize=size;
		this.ysize=size;
		this.minX = stageMinX + (int) size / 2;
		this.minY = stageMinY + (int) size / 2;
		this.maxX = stageMaxX - (int) size / 2;
		this.maxY = stageMaxY - (int) size / 2;
		this.res = 0.5;
		this.hp = 100;
		this.mhp=hp;
		this.oldHP = hp;
	}

	public void update() {
		if (status == hop) {
			hitStage();
			double length = Math.sqrt((vx * vx) + (vy * vy));
			if (length > 0)
				length = 1 / length;
			double dx = vx * length;
			double dy = vy * length;
			
			// すべてのFurniture.wallに対して実行
			Iterator<Furniture> fitr = fnts.iterator();
			while (fitr.hasNext()) {
				Iterator<WallObject> witr = fitr.next().walls.iterator();
				while (witr.hasNext()) {
					WallObject tmp=witr.next();
					if(hitWall(tmp)){
						//HPを減らす処理が上手にできない
						//Wallの上にオブジェクトが乗っているとき、ずっとHPが減ってしまう
						tmp.hp -= this.m*vSize();
					}
				}
			}
			vy += G;
			x += vx;
			y += vy;

			// 移動量の単位ベクトル
			if(vx>0)
				rad +=vSize();
			if(vx<0)
				rad-=vSize();
			
			// すべてのbrkに対して実行
			Iterator<BreakedObject> bitr = brks.iterator();
			while (bitr.hasNext())

			{

				BreakedObject tmp = bitr.next();
				if (!tmp.equals(this)) {
					if(FallonBrks(tmp)){
						this.hp -= tmp.m * tmp.vSize();
						tmp.hp -= this.m * this.vSize();
					}
				}
			}
		}else if(status==stop){
		}
		
		impact = hp - oldHP;
		oldHP = hp;
		updateAfter();
	}
	public void updateAfter(){
		
	}

	public void draw(Graphics g) {
		double drawX = x - offsetX;
		double drawY = y - offsetY;
		Graphics2D g2 = (Graphics2D) g;
		if (status == stop) {
			g2.setColor(Color.BLUE);
			AffineTransform at = new AffineTransform();
			at.setToRotation(Math.toRadians(rad), drawX, drawY);
			g2.setTransform(at);
			g2.fillRect((int) (drawX - size / 2), (int) (drawY - size / 2), (int) size, (int) size);
			at = new AffineTransform();
			g2.setTransform(at);
		} else if (status == hop) {
			g2.setColor(Color.red);
			AffineTransform at = new AffineTransform();
			at.setToRotation(Math.toRadians(rad), drawX, drawY);
			g2.setTransform(at);
			g2.fillRect((int) (drawX - size / 2), (int) (drawY - size / 2), (int) size, (int) size);
			at = new AffineTransform();
			g2.setTransform(at);
		}
		g.setColor(Color.yellow);
		g.drawOval((int) (drawX - size / 2), (int) (drawY - size / 2), (int) size, (int) size);
		g.setColor(Color.WHITE);
		g.fillRect((int) drawX, (int) drawY - 5, (int) hp, 10);
	}

	public boolean checkFallon(BreakedObject brk) {
		return Math.pow(this.x - brk.x, 2) + Math.pow(this.y - brk.y, 2) < Math.pow(this.size / 2 + brk.size / 2, 2);
	}

	public void hitStage(){
		if (x < minX)

		{
			x = minX;
			hp -= this.m * vSize();
			vx *= -0.5;
		}
		if (x > maxX)

		{
			x = maxX;
			hp -= this.m * vSize();
			vx *= -0.5;
		}
		if (y < minY)

		{
			y = minY;
			hp -= this.m * vSize();
			vy *= -0.5;
		}
		if (y > maxY)

		{
			y = maxY;
			vy *= -0.5;
			vx *= 1;
		}
	}
	public boolean hitWall(WallObject tmp) {
		// 玉から面と垂直な線を出して面と交差するか調べる
		tmp.sx = -tmp.nx * this.size / 2;
		tmp.sy = -tmp.ny * this.size / 2;
		double d = -(tmp.x1 * tmp.nx + tmp.y1 * tmp.ny);
		double t = -(tmp.nx * x + tmp.ny * y + d) / (tmp.nx * tmp.sx + tmp.ny * tmp.sy);

		if (t > 0 && t <= 1) {

			// 交点が線分の中に入っているか調べる
			double cx = x + t * tmp.sx;
			double cy = y + t * tmp.sy;
			double acx = cx - tmp.x1;
			double acy = cy - tmp.y1;

			double bcx = cx - tmp.x2;
			double bcy = cy - tmp.y2;

			if ((acx * bcx) + (acy * bcy) <= 0) {

				this.x = cx + this.size / 2 * tmp.nx;
				this.y = cy + this.size / 2 * tmp.ny;

				// 反射ベクトル
				double t1 = -(tmp.nx * vx + tmp.ny * vy) / (tmp.nx * tmp.nx + tmp.ny * tmp.ny);
				vx += t1 * tmp.nx * 2 * amp;
				vy += t1 * tmp.ny * 2 * amp;
				
				return true;
			}

			// 円が始点と重なっているか調べる ---------------------------------
			double vx2 = this.x - tmp.x1;
			double vy2 = this.y - tmp.y1;
			if (vx2 * vx2 + vy2 * vy2 < this.size / 2 * this.size / 2) {
				double length1 = Math.sqrt((vx2 * vx2) + (vy2 * vy2));
				if (length1 > 0)
					length1 = 1 / length1;
				vx2 = vx2 * length1;
				vy2 = vy2 * length1;

				// 位置補正
				this.x = tmp.x1 + vx2 * this.size / 2;
				this.y = tmp.y1 + vy2 * this.size / 2;
				
				

				// 反射方向
				double t1 = -(vx2 * vx + vy2 * vy) / (vx2 * vx2 + vy2 * vy2);
				vx += t1 * vx2 * 2 * amp;
				vy += t1 * vy2 * 2 * amp;
				
				return true;
			}
			// 円が終点と重なっているか調べる
			double vx1 = this.x - tmp.x2;
			double vy1 = this.y - tmp.y2;
			if (vx1 * vx1 + vy1 * vy1 < this.size / 2 * this.size / 2) {
				double length1 = Math.sqrt((vx1 * vx1) + (vy1 * vy1));
				if (length1 > 0)
					length1 = 1 / length1;
				vx1 = vx1 * length1;
				vy1 = vy1 * length1;

				// 位置補正
				this.x = tmp.x2 + vx1 * this.size / 2;
				this.y = tmp.y2 + vy1 * this.size / 2;

				// 反射方向
				double t1 = -(vx1 * vx + vy1 * vy) / (vx1 * vx1 + vy1 * vy1);
				vx += t1 * vx1 * 2 * amp;
				vy += t1 * vy1 * 2 * amp;
				
				return true;
			}
		}
		return false;
	}

	public boolean FallonBrks(BreakedObject brk) {
		// 円同士が衝突するか判定
		// dx->vx this.x->x
		double _a = (this.vx * this.vx) - 2 * (this.vx * brk.vx) + (brk.vx * brk.vx) + (this.vy * this.vy)
				- 2 * (this.vy * brk.vy) + (brk.vy * brk.vy);
		double _b = 2 * (this.x * this.vx) - 2 * (this.x * brk.vx) - 2 * (this.vx * brk.x) + 2 * (brk.x * brk.vx)
				+ 2 * (this.y * this.vy) - 2 * (this.y * brk.vy) - 2 * (this.vy * brk.y) + 2 * (brk.y * brk.vy);
		double _c = (this.x * this.x) - 2 * (this.x * brk.x) + (brk.x * brk.x) + (this.y * this.y)
				- 2 * (this.y * brk.y) + (brk.y * brk.y)
				- (this.size / 2 + brk.size / 2) * (this.size / 2 + brk.size / 2);
		double _d = _b * _b - 4 * _a * _c;

		if (_d <= 0) {
		} else {
			_d = Math.sqrt(_d);
			double f0 = (-_b - _d) / (2 * _a); // 接触する瞬間
			double f1 = (-_b + _d) / (2 * _a); // 離れる瞬間

			boolean hit = false;

			// 現在のフレームで衝突するので補正
			if (0 <= f0 && f0 <= 1) {

				this.x = this.x + this.vx * f0;
				this.y = this.y + this.vy * f0;
				brk.x = brk.x + brk.vx * f0;
				brk.y = brk.y + brk.vy * f0;
				hit = true;

				// めり込んでいるので単純補正
			} else if (f0 * f1 < 0) {

				double vx = (this.x - brk.x);
				double vy = (this.y - brk.y);
				double len = Math.sqrt(vx * vx + vy * vy);
				double distance = this.size / 2 + brk.size / 2 - len;

				if (len > 0)
					len = 1 / len;
				vx *= len;
				vy *= len;

				distance /= 2.0;
				this.x += vx * distance;
				this.y += vy * distance;
				brk.x -= vx * distance;
				brk.y -= vy * distance;

				hit = true;

			}

			// 衝突後の速度を計算
			if (hit) {
				brk.status = hop;

				// 速度を移動成分と回転成分に分離
				double t;
				double vx = (brk.x - this.x);
				double vy = (brk.y - this.y);

				t = -(vx * this.vx + vy * this.vy) / (vx * vx + vy * vy);
				double arx = this.vx + vx * t;
				double ary = this.vy + vy * t;

				t = -(-vy * this.vx + vx * this.vy) / (vy * vy + vx * vx);
				double amx = this.vx - vy * t;
				double amy = this.vy + vx * t;

				t = -(vx * brk.vx + vy * brk.vy) / (vx * vx + vy * vy);
				double brx = brk.vx + vx * t;
				double bry = brk.vy + vy * t;

				t = -(-vy * brk.vx + vx * brk.vy) / (vy * vy + vx * vx);
				double bmx = brk.vx - vy * t;
				double bmy = brk.vy + vx * t;

				// 移動成分同士の衝突後のベクトルを計算
				double e = 1;
				double adx = (this.m * amx + brk.m * bmx + bmx * e * brk.m - amx * e * brk.m) / (this.m + brk.m);
				double bdx = -e * (bmx - amx) + adx;
				double ady = (this.m * amy + brk.m * bmy + bmy * e * brk.m - amy * e * brk.m) / (this.m + brk.m);
				double bdy = -e * (bmy - amy) + ady;

				// 回転成分を加算して衝突後の速度を計算

				this.vx = adx + arx;
				this.vy = ady + ary;
				brk.vx = bdx + brx;
				brk.vy = bdy + bry;

				//
				this.vx*=amp;
				this.vy*=amp;
				brk.vx*=amp;
				brk.vy*=amp;
				return true;
			}
		}
		return false;
	}

	public void mouseDamaged(double x, double y, double speed) {
		this.status = hop;
		double dx = this.x - offsetX - x;
		double dy = this.y - offsetY - y;
		this.vx += speed * dx / (Math.abs(dx) + Math.abs(dy));
		this.vy += speed * dy / (Math.abs(dx) + Math.abs(dy));
		// this.hp -= vSize();

	}

	public void setPoint(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public void setRad(float rad) {
		this.rad = rad;
	}

	public double vSize() {
		return Math.sqrt(vx * vx + vy * vy);
	}

	public void setOffset(int offsetX, int offsetY) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}

	public double getImpact() {
		return this.impact;
	}
	
	public void destructor(ArrayList<BreakedObject> brks,int minX,int minY,int maxX,int maxY){
	}
}
