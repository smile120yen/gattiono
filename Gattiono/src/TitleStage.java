import java.awt.Graphics;
import java.util.HashMap;
import java.util.Iterator;

public class TitleStage extends Stage{
	Images images;
	HashMap<String, Button> buttons;
	BreakedObject start;
	public TitleStage(Images images,HashMap<String,Stage> stages,Score score,IOStatus io){	
		super(images,stages,score,io);
		this.name="Sample";
		this.images=images;
		images.loadImage("wall_long");
		this.maxX=(images.get("wall_long").getWidth() - 1) * 7;
		this.maxY=images.get("wall_long").getHeight()-100;
		this.width=maxX-minX;
		this.height=maxY-minY;
		
		fnts.add(new Table(fnts,280,520,images));
		start=new ImageObject("signboard_start",lists,400,500,minX,minY,maxX,maxY,images);
		brks.add(start);
		
		
		buttons = new HashMap<String, Button>();
		
		offsetY=this.getMinY()+this.getHeight()/4;
		offsetX=this.getMinX();
		
	}
	public Stage update(){
		updateOffset();
		updateObjects();
		updateHand();
		Iterator<Button> itr = buttons.values().iterator();
		while (itr.hasNext()) {
			itr.next().update();
		}
		if(start.status==BreakedObject.hop){
			changeScene=true;
		}
		
		return this;
	}
	public void draw(Graphics g){
		int nOffsetX=offsetX;
		int nOffsetY=offsetY;
		for (int i = 0; i < 7; i++) {
			g.drawImage(images.get("wall_long"), (images.get("wall_long").getWidth() - 1) * i-nOffsetX ,-nOffsetY,null);
		}
		
		Iterator<Button> buttonItr = buttons.values().iterator();
		while (buttonItr.hasNext()) {
			Button tmp=buttonItr.next();
			tmp.setOffset(nOffsetX,nOffsetY);
			tmp.draw(g);
		}
		
		drawObjects(g);
		hand.draw(g);
	}
}