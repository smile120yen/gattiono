import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class Tansu extends SoftRectWall {
	Images images;

	public Tansu(ArrayList<Furniture> fnts, int x, int y, Images images) {
		super(fnts, /* HP */20, x, y, 319, 500);
		this.xsize=319;
		this.ysize=500;
		this.images = images;
		images.loadImage("tansu");
		images.loadImage("tansu_damage");
		color=new Color(164,88,0);
	}

	public void draw(Graphics g) {
		if (getImpact() < -10) {
			g.translate((int) (-10 * Math.random()) + 10 / 2, (int) (-10 * Math.random()) + 10 / 2);
		} else {
			g.translate((int) (impact  * Math.random() + impact  / 2), (int) (impact  * Math.random() + impact / 2));
		}
		
		g.drawImage(images.get("tansu"), x - offsetX, y - offsetY, null);
		Graphics2D g2=(Graphics2D)g;
	}
}
