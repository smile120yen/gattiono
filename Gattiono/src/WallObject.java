import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class WallObject {
	public int x1, y1, x2, y2;
	double hp,mhp;
	protected double oldHP;
	public double length, nx, ny, sx, sy;
	public int offsetX, offsetY;
	double impact;
	boolean breakable = false;

	public WallObject(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.sx = x2 - x1;
		this.sy = y2 - y1;
		this.length = Math.sqrt((sx * sx) + (sy * sy));
		if (length > 0)
			length = 1 / length;
		sx *= length;
		sy *= length;
		this.nx = -(sy);
		this.ny = sx;
		this.oldHP = hp;
		this.mhp=hp;
	}

	public void update() {
		impact =hp - oldHP;
		oldHP = hp;
	}

	public void draw(Graphics g) {
		g.setColor(Color.red);
		g.drawLine(x1 - offsetX, y1 - offsetY, x2 - offsetX, y2 - offsetY);

		g.setColor(Color.WHITE);
		g.fillRect((int) x1 - offsetX, (int) y1 - offsetY, (int) hp, 10);
	}

	public void setOffset(int offsetX, int offsetY) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}

	public void setBreakable(int hp) {
		this.breakable = true;
		this.mhp = hp;
		this.hp = hp;
	}

	public boolean checkBreak(){
		if(breakable){
			return hp<=0;
		}
		return false;
	}

	public double getImpact() {
		return this.impact;
	}
}
