import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

public class Hand {
	Images images;
	int mouseStatus, mouseX, mouseY, size = 30, offsetX, offsetY;
	double charge = 0;
	int vivX = 0, vivY = 0;
	ArrayList<BreakedObject> brks;
	Stage currentStage;
	static final int IMPACT = 0, CHOICE = 1, PUSH = 2;
	int status = IMPACT;

	public Hand(Stage currentStage, Images images) {
		this.currentStage = currentStage;
		this.brks = currentStage.brks;
		this.images = images;
		images.loadImage("hand_open");
		images.loadImage("hand_close");
		images.loadImage("hand_choice");
		images.loadImage("hand_push");
	}

	public void update(IOStatus io) {
		this.mouseStatus = io.mouseStatus;
		this.mouseX = io.mouseX;
		this.mouseY = io.mouseY;
		if (io.keyStatus.get("shift") == 1) {
			if (mouseStatus == 2) {
				this.status = PUSH;
			} else {
				this.status = CHOICE;
			}
		} else {
			this.status = IMPACT;
		}

		if (this.status == IMPACT) {
			if (mouseStatus != 0) {
				if (charge < 100) {
					charge += 0.1;
				}
			}
			if (mouseStatus == 0 && charge > 0) {
				Iterator<BreakedObject> itrb = brks.iterator();
				while (itrb.hasNext()) {
					BreakedObject tmp = itrb.next();
					if (checkFallon(tmp)) {
						tmp.mouseDamaged(mouseX, mouseY, charge / 20 / tmp.m);
					}
				}
				charge = 0;
			}
		}else if(this.status==CHOICE){
			charge=0;
		}else if(this.status==PUSH){
			charge=0;
		}
	}

	public void draw(Graphics g) {

		if (status == IMPACT) {
			g.setColor(Color.yellow);
			g.fillOval(mouseX - (size + (int) charge) / 2, mouseY - (size + (int) charge) / 2, size + (int) charge,
					size + (int) charge);
			if (charge == 0) {
				g.drawImage(images.get("hand_open"), mouseX - images.get("hand_open").getWidth() / 2,
						mouseY - images.get("hand_open").getHeight() / 2, null);
			} else if (charge > 0) {
				vivX = (int) (Math.random() * 6 - 3);
				vivY = (int) (Math.random() * 6 - 3);
				g.drawImage(images.get("hand_close"), mouseX - images.get("hand_close").getWidth() / 2 + vivX,
						mouseY - images.get("hand_close").getHeight() / 2 + vivY, null);
			}
		} else if (status == CHOICE) {
			g.drawImage(images.get("hand_choice"), mouseX - images.get("hand_choice").getWidth() / 2,
					mouseY - images.get("hand_choice").getHeight() / 2, null);
		} else if (status == PUSH) {
			g.drawImage(images.get("hand_push"), mouseX - images.get("hand_push").getWidth() / 2,
					mouseY - images.get("hand_push").getHeight() / 2, null);
		}
	}

	public boolean checkFallon(BreakedObject brk) {
		return Math.pow(this.mouseX - brk.x + offsetX, 2) + Math.pow(this.mouseY - brk.y + offsetY, 2) < Math
				.pow((this.size + (int) charge) / 2 + brk.size / 2, 2);
	}

	public void setOffset(int offsetX, int offsetY) {
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}
}
