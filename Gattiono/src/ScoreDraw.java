import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class ScoreDraw {
	int x,y,offsetX,offsetY;
	public int score,time=2550;
	public ScoreDraw(int score,int x,int y){
		this.x=x;
		this.y=y;
		this.score=score;
	}
	public void update(){
		time-=3;
	}
	public void draw(Graphics g){
		if(score<0){
			g.setColor(new Color(255,0,0,time/10));
			g.setFont(new Font("Franklin Gothic Heavy", 10, 50));
		}else{
			g.setColor(new Color(0,0,0,time/10));
			g.setFont(new Font("Franklin Gothic Heavy", 10, 20));
		}
		g.drawString("+"+score, x-offsetX, y-offsetY+time/200);
	}
	public void setOffset(int x,int y){
		this.offsetX=x;
		this.offsetY=y;
	}
	
}
