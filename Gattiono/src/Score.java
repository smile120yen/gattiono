import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

public class Score {
	protected int score=0,x=0,y=0;
	Images images;
	//消えるまでの時間　座標　スコアの値　を持つ構造体
	ArrayList<ScoreDraw> draws=new ArrayList<ScoreDraw>();
	public Score(Images images){
		this.images=images;
		images.loadImage("0");
		images.loadImage("1");
		images.loadImage("2");
		images.loadImage("3");
		images.loadImage("4");
		images.loadImage("5");
		images.loadImage("6");
		images.loadImage("7");
		images.loadImage("8");
		images.loadImage("9");
	}
	public void setDrawPoint(int x,int y){
		this.x=x;
		this.y=y;
	}
	public void update(){
		Iterator<ScoreDraw> sitr = draws.iterator();
		while (sitr.hasNext()) {
			ScoreDraw tmp=sitr.next();
			tmp.update();
			if(tmp.time<0){
				sitr.remove();
			}
		}
	}
	public void draw(Graphics g){
		char[] scoreArray=new Integer(score).toString().toCharArray();
		int offset=0;
		for(int i=0; i<scoreArray.length; i++){
			g.drawImage(images.get(String.valueOf(scoreArray[i])), x+offset, y, null);
			offset+=images.get(String.valueOf(scoreArray[i])).getWidth();
		}
		
		for(int i=0; i<draws.size(); i++){
			draws.get(i).draw(g);
		}
	}
	public void addScore(int add,int x,int y){
		if(add!=0)
		draws.add(new ScoreDraw(add,x,y));
		this.score+=add;
		if(score<0)score=0;
	}
	public void setOffset(int x,int y){
		for(int i=0; i<draws.size(); i++){
			draws.get(i).setOffset(x, y);
		}
	}
}
