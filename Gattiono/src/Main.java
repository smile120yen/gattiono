import javax.swing.JFrame;

public class Main{
	public static void main(String[] args) {
		JFrame frame = new JFrame("タイトル");
		
		frame.setTitle("タイトル");
		frame.setBounds(100, 100, DFN.width, DFN.height);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MyPanel myPanel=new MyPanel();
		frame.getContentPane().add(myPanel);
		myPanel.start();
		frame.setVisible(true);
	}
	
	
}