import java.util.HashMap;

public class IOStatus {
	public int mouseStatus = 0, mouseX, mouseY;
	public HashMap<String, Integer> keyStatus;
	public IOStatus(){
		keyStatus = new HashMap<String, Integer>();
		keyStatus.put("up", 0);
		keyStatus.put("down", 0);
		keyStatus.put("left", 0);
		keyStatus.put("right", 0);
		keyStatus.put("shift", 0);
	}
}
