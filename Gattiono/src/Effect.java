import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

public class Effect extends BreakedObject{
	Color color;
	public Effect(ArgList lists, float x, float y, float size, int stageMinX,
			int stageMinY, int stageMaxX, int stageMaxY,Color color) {
		super(lists, x, y, size, stageMinX, stageMinY, stageMaxX, stageMaxY);
		this.color=color;
		this.hp=500000;
		this.mhp=hp;
		// TODO Auto-generated constructor stub
		this.status=hop;
		amp=1;
		res=1.2;
		addScore=(int)size;
	}
	public void updateAfter(){
		y+=0.1;
		if(y>maxY-50){
			this.hp-=1000*y/maxY;
		}
	}
	public void draw(Graphics g) {
		double drawX = x - offsetX;
		double drawY = y - offsetY;
		Graphics2D g2 = (Graphics2D) g;
		if (status == stop) {
			g2.setColor(color);
			AffineTransform at = new AffineTransform();
			at.setToRotation(Math.toRadians(rad), drawX, drawY);
			g2.setTransform(at);
			g2.fillRect((int) (drawX - size / 2), (int) (drawY - size / 2), (int) size, (int) size);
			at = new AffineTransform();
			g2.setTransform(at);
		} else if (status == hop) {
			g2.setColor(color);
			AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,Math.max((float)(hp/mhp),0));
			g2.setComposite(ac);
			AffineTransform at = new AffineTransform();
			at.setToRotation(Math.toRadians(rad), drawX, drawY);
			g2.setTransform(at);
			g2.fillRect((int) (drawX - size / 2), (int) (drawY - size / 2), (int) size, (int) size);
			at = new AffineTransform();
			g2.setTransform(at);
			ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,1);
			g2.setComposite(ac);
		}
	}
	public double getImpact() {
		return 0;
	}
	public void destructor(ArrayList<BreakedObject> brks,int minX,int minY,int maxX,int maxY){
		
	}
}
