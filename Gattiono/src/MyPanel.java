import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.HashMap;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MyPanel extends JPanel implements Runnable, MouseListener, KeyListener, MouseMotionListener,MouseWheelListener {
	int x = 0;
	Thread gameLoop;
	Scene currentScene;
	IOStatus io;

	public MyPanel() {
		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);
		io=new IOStatus();
		currentScene = new TitleScene(io);
	}

	public void paintComponent(Graphics g) {
		try {
			currentScene.draw(g);
		} catch (NullPointerException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "シーンが存在しません", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}

	private void update(){
		requestFocusInWindow();
		
		try {
			currentScene = currentScene.update();
		} catch (NullPointerException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "シーンが存在しません", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		if (io.mouseStatus == 1)
			io.mouseStatus = 2;
	}

	public void start() {
		gameLoop = new Thread(this);
		gameLoop.start();
	}

	@Override
	public void run() {
		while (true) {
			repaint();
			update();
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		io.mouseStatus = 1;
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		io.mouseStatus = 0;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		int keyCode = arg0.getKeyCode();
		switch (keyCode) {
		case KeyEvent.VK_W:
			io.keyStatus.put("up", 1);
			break;
		case KeyEvent.VK_A:
			io.keyStatus.put("left", 1);
			break;
		case KeyEvent.VK_S:
			io.keyStatus.put("down", 1);
			break;
		case KeyEvent.VK_D:
			io.keyStatus.put("right", 1);
			break;
		case KeyEvent.VK_SHIFT:
			io.keyStatus.put("shift", 1);
			break;
		}
		/*
		if(arg0.getModifiers() == KeyEvent.SHIFT_MASK){
			io.keyStatus.put("shift", 1);
		}*/

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		int keyCode = arg0.getKeyCode();
		switch (keyCode) {
		case KeyEvent.VK_W:
			io.keyStatus.put("up", 0);
			break;
		case KeyEvent.VK_A:
			io.keyStatus.put("left", 0);
			break;
		case KeyEvent.VK_S:
			io.keyStatus.put("down", 0);
			break;
		case KeyEvent.VK_D:
			io.keyStatus.put("right", 0);
			break;
		case KeyEvent.VK_SHIFT:
			io.keyStatus.put("shift", 0);
			break;
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		this.io.mouseX = arg0.getX();
		this.io.mouseY = arg0.getY();
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		this.io.mouseX = arg0.getX();
		this.io.mouseY = arg0.getY();

	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
