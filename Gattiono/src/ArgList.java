import java.util.ArrayList;
import java.util.HashMap;

public class ArgList {
	public ArrayList<BreakedObject> brks;
	public HashMap<String, Button> buttons;
	public ArrayList<BreakedObject> effects;
	public ArrayList<Furniture> fnts;
	public Sounds sounds;

	public ArgList(ArrayList<BreakedObject> brks, HashMap<String, Button> buttons, ArrayList<BreakedObject> effects,
			ArrayList<Furniture> fnts,Sounds sounds) {
		this.brks=brks;
		this.buttons=buttons;
		this.effects=effects;
		this.fnts=fnts;
		this.sounds=sounds;
	}
}
