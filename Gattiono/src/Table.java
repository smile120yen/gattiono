import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

public class Table extends SimpleWall {
	Images images;

	public Table(ArrayList<Furniture> fnts, int x, int y, Images images) {
		super(fnts, x, y, x+234, y);
		this.images = images;
		images.loadImage("table");
		//images.loadImage("table_damage");
	}

	public void draw(Graphics g) {
		
		g.drawImage(images.get("table"), x - offsetX, y - offsetY, null);
		/*
		Graphics2D g2=(Graphics2D)g;
		AlphaComposite ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER,(float)0.8*(1-getParthHP()));
		g2.setComposite(ac);
		g2.drawImage(images.get("table_damage"), x - offsetX, y - offsetY, null);
		ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f);
		g2.setComposite(ac);*/
	}
}