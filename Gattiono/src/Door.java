import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Door extends Button{
	ArrayList<BufferedImage> animation;
	int animationPoint=0,time=0;
	public boolean endAnimation;
	boolean switchAnimation;
	ArgList lists;
	public Door(ArgList lists,IOStatus io,Images images) {
		super(io);
		
		animation=new ArrayList<BufferedImage>();
		images.loadImage("door");
		images.loadImage("door_open1");
		images.loadImage("door_open2");
		images.loadImage("door_open3");
		images.loadImage("door_open4");
		
		animation.add(images.get("door_open1"));
		animation.add(images.get("door_open2"));
		animation.add(images.get("door_open3"));
		animation.add(images.get("door_open4"));
		animation.add(images.get("door_open3"));
		animation.add(images.get("door_open2"));
		animation.add(images.get("door_open1"));
		animation.add(images.get("door"));
		
		this.setImage(images.get("door"));
		this.lists=lists;
		this.lists.sounds.load("door_open","door-open1.wav");
		this.lists.sounds.load("door_close","door-close1.wav");
	}
	public void update() {
		if(switchAnimation){
			this.setImage(animation.get(animationPoint));
			time++;
			if(time>50){
				if(animationPoint==0){
					lists.sounds.start("door_open");
				}
				if(animationPoint==4){
					lists.sounds.start("door_close");
				}
				animationPoint++;
				time=0;
			}
			if(animationPoint==animation.size()){
				switchAnimation=false;
				endAnimation=true;
			}
		}
	}
	public void startAnimation(){
		switchAnimation=true;
	}
}
