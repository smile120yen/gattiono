import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

public class GameScene extends Scene {
	Images images;
	Stage currentStage;
	int impact,size;
	static final int maxSize=(int)Math.sqrt(DFN.width*DFN.width+DFN.height*DFN.height);
	HashMap<String, Stage> stages;
	static Score score;
	Time time;

	public GameScene(IOStatus io) {
		super(io);
		images = new Images();
		
		score=new Score(images);
		score.setDrawPoint(10, 10);
		
		time=new Time(images);
		time.setDrawPoint(700,10);

		stages = new HashMap<String, Stage>();
		stages.put("Sample", new SampleStage(images, stages, score,io));
		stages.put("Sample2", new SampleStage2(images, stages,score, io));

		currentStage = stages.get("Sample");

		name = new String("Game");
		images.loadImage("head");
	}

	public Scene update() {
		currentStage = currentStage.update();

		impact += 1;
		if (impact > 0)
			impact = 0;
		if (impact < -20) {
			impact = -20;
		}
		impact += currentStage.getImpact() * 5;

		if(size<=maxSize){
			size+=2;
		}
		
		time.update();
		return this;
	}

	public void draw(Graphics g) {
		if (impact < -10) {
			g.translate((int) (-10 * Math.random()) + 10 / 2, (int) (-10 * Math.random()) + 10 / 2);
		} else {
			g.translate((int) (impact * Math.random()) + impact / 2, (int) (impact * Math.random()) + impact / 2);
		}
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, DFN.width, DFN.height);

		currentStage.draw(g);
		g.drawImage(images.get("head"), 0, 30 - currentStage.offsetY / 10, null);

		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.black);
		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g2.setFont(new Font("MS Gothic", 40, 40));
	
		score.draw(g);
		time.draw(g);
		
		if(size<maxSize){
			drawEnd(g,size);
		}
		
		
	}
	public void drawEnd(Graphics g,int size){
		
		Graphics2D g2 = (Graphics2D)g;
	    Rectangle2D e1 = new Rectangle2D.Double(0,0,DFN.width,DFN.height);
	    Ellipse2D e2 = new Ellipse2D.Double(DFN.width/2-size/2,DFN.height/2-size/2,size,size);
	    Area a1 = new Area(e1);
	    Area a2 = new Area(e2);
	    a1.subtract(a2);
	    g2.setColor(Color.BLACK);
	    g2.fill(a1);
	}
}
