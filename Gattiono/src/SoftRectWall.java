import java.util.ArrayList;
import java.util.Iterator;

public class SoftRectWall extends Furniture {

	public SoftRectWall(ArrayList<Furniture> fnts, int hp, int x, int y, int xsize, int ysize) {
		super(fnts, x, y);
		// TODO Auto-generated constructor stub
		walls.add(new WallObject(x + xsize, y, x, y));
		walls.add(new WallObject(x, y, x, y + ysize));
		walls.add(new WallObject(x, y + ysize, x + xsize, y + ysize));
		walls.add(new WallObject(x + xsize, y + ysize, x + xsize, y));
		Iterator<WallObject> itr = walls.iterator();
		while (itr.hasNext()) {
			itr.next().setBreakable(hp);
		}
	}

	public boolean checkBreak() {
		Iterator<WallObject> itr = walls.iterator();
		while (itr.hasNext()) {
			if (itr.next().checkBreak()) {
				return true;
			}
		}
		return false;
	}
}
