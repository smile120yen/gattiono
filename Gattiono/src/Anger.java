import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

public class Anger extends BreakedObject{

	public Anger(ArgList lists, float x, float y, float size, int stageMinX,
			int stageMinY, int stageMaxX, int stageMaxY) {
		super(lists, x, y, size, stageMinX, stageMinY, stageMaxX, stageMaxY);
			this.hp=0;
			this.mhp=hp;
			// TODO Auto-generated constructor stub
			this.status=hop;
			amp=1;
			res=1.2;
			addScore=-100;
		}
		public void updateAfter(){
		}
		public void draw(Graphics g) {
			double drawX = x - offsetX;
			double drawY = y - offsetY;
			Graphics2D g2 = (Graphics2D) g;
			if (status == stop) {
				AffineTransform at = new AffineTransform();
				at.setToRotation(Math.toRadians(rad), drawX, drawY);
				g2.setTransform(at);
				
				g.fillRect((int)x,(int)y,(int)size,(int)size);
				
				g2.setTransform(new AffineTransform());
			} else if (status == hop) {
				AffineTransform at = new AffineTransform();
				at.setToRotation(Math.toRadians(rad), drawX, drawY);
				g2.setTransform(at);
				
				g.fillRect((int)x,(int)y,(int)size,(int)size);
				
				g2.setTransform(new AffineTransform());
			}
		}
		public double getImpact() {
			return 0;
		}
		public void destructor(ArrayList<BreakedObject> brks,int minX,int minY,int maxX,int maxY){
			
		}

}
