import java.awt.Graphics;
import java.util.HashMap;
import java.util.Iterator;

public class SampleStage extends Stage{
	Images images;
	public SampleStage(Images images,HashMap<String,Stage> stages,Score score,IOStatus io){	
		super(images,stages,score,io);
		this.name="Sample";
		this.images=images;
		images.loadImage("wall_long");
		this.maxX=(images.get("wall_long").getWidth() - 1) * 10;
		this.maxY=images.get("wall_long").getHeight()-100;
		this.width=maxX-minX;
		this.height=maxY-minY;
		
		
		fnts.add(new Tansu(fnts,200,200,images));
		brks.add(new Book(lists,250,305,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,280,305,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,310,305,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,340,305,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,370,305,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,400,305,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,430,305,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,460,305,minX,minY,maxX,maxY,images));
		
		brks.add(new Book(lists,250,460,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,280,460,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,310,460,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,340,460,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,370,460,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,400,460,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,430,460,minX,minY,maxX,maxY,images));
		brks.add(new Book(lists,460,460,minX,minY,maxX,maxY,images));
		
		brks.add(new Human(lists,600,500,minX,minY,maxX,maxY,images));
	
		buttons.put("door", new Door(lists,io,images));
		buttons.get("door").setPoint(900, 130);
		
		offsetY=this.getMinY()+this.getHeight()/4;
		offsetX=this.getMinX()+this.getWidth()/4;
		
	}
	public Stage update(){
		updateOffset();
		updateObjects();
		updateHand();
		Iterator<Button> itr = buttons.values().iterator();
		while (itr.hasNext()) {
			itr.next().update();
		}
		
		if(buttons.get("door").checkClick()){
			((Door) buttons.get("door")).startAnimation();
		}
		if(((Door)buttons.get("door")).endAnimation){
			return stages.get("Sample2");
		}
	
		
		return this;
	}
	public void draw(Graphics g){
		int nOffsetX=offsetX;
		int nOffsetY=offsetY;
		for (int i = 0; i < 10; i++) {
			g.drawImage(images.get("wall_long"), (images.get("wall_long").getWidth() - 1) * i-nOffsetX ,-nOffsetY,null);
		}
		
		Iterator<Button> buttonItr = buttons.values().iterator();
		while (buttonItr.hasNext()) {
			Button tmp=buttonItr.next();
			tmp.setOffset(nOffsetX,nOffsetY);
			tmp.draw(g);
		}
		
		drawObjects(g);
		hand.draw(g);
	}
}
