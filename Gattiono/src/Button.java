import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class Button {
	int x, y, type, xsize, ysize, size;
	int offsetX, offsetY;
	BufferedImage image;
	final static int square = 0, circle = 1;
	IOStatus io;
	public Button(IOStatus io) {
		this.io=io;
	}

	public void setPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
		this.xsize = image.getWidth();
		this.ysize = image.getHeight();
	}

	public void setType(String type) {
		if (type.equals("Circle")) {
			this.type = circle;
		}
		if (type.equals("Scuare")) {
			this.type = square;
		}
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setSize(int xsize, int ysize) {
		this.xsize = xsize;
		this.ysize = ysize;
	}

	public void update() {
		
	}

	public void draw(Graphics g) {
		int nx = x - offsetX;
		int ny = y - offsetY;
		if (image != null) {
			g.drawImage(image, nx, ny, null);
		} else {
			g.setColor(Color.red);
			if (checkOn()) {
				g.setColor(Color.YELLOW);
			}
			if (checkPush()) {
				g.setColor(Color.BLUE);
			}
			if (checkClick()) {
				g.setColor(Color.green);
			}
			switch (type) {
			case square:
				g.fillRect(nx, ny, xsize, ysize);
				break;
			case circle:
				g.fillOval(nx - size / 2, ny - size / 2, size, size);
				break;
			}
		}
	}

	public boolean checkOn() {
		// マウスが乗っているならTrue
		int nx = x - offsetX;
		int ny = y - offsetY;
		switch (type) {
		case square:
			if (nx < io.mouseX && io.mouseX < nx + xsize && ny < io.mouseY && io.mouseY < ny + ysize) {
				return true;
			} else {
				return false;
			}
		case circle:
			if (Math.pow(nx - io.mouseX, 2) + Math.pow(ny - io.mouseY, 2) < Math.pow(size / 2, 2)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	public boolean checkClick() {
		// クリックされた瞬間ならTrue
		if (checkOn() && io.mouseStatus == 1&&io.keyStatus.get("shift")!=0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkPush() {
		// 押されているならTrue
		if (checkOn() && io.mouseStatus != 0&&io.keyStatus.get("shift")!=0) {
			return true;
		} else {
			return false;
		}
	}

	public void setOffset(int offsetX, int offsetY) {
		// TODO Auto-generated method stub
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}
}
