import java.awt.Graphics;
import java.util.HashMap;
import java.util.Iterator;

public class SampleStage2 extends Stage{
	Images images;
	HashMap<String, Button> buttons;
	int changeStage=0;
	final static int None=0,Sample=1;
	public SampleStage2(Images images,HashMap<String,Stage> stages,Score score,IOStatus io){
		super(images,stages,score,io);
		this.name="Sample2";
		this.images=images;
		images.loadImage("wall_long");
		this.maxX=(images.get("wall_long").getWidth() - 1) * 10;
		this.maxY=images.get("wall_long").getHeight()-100;
		this.width=maxX-minX;
		this.height=maxY-minY;
		
		buttons = new HashMap<String, Button>();
		
		/*
		buttons.put("upStairs", new Button(io));
		buttons.get("upStairs").setPoint(900, 165);
		images.loadImage("upStairs");
		buttons.get("upStairs").setImage(images.get("upStairs"));
		*/
		
		offsetY=this.getMinY()+this.getHeight()/4;
		offsetX=this.getMinX()+this.getWidth()/4;
		
	}
	public Stage update(){
		updateOffset();
		updateObjects();
		updateHand();
		Iterator<Button> itr = buttons.values().iterator();
		while (itr.hasNext()) {
			itr.next().update();
		}
		/*
		if(buttons.get("upStairs").checkClick()){
			return stages.get("Sample");
		}*/
		return this;
	}
	public void draw(Graphics g){
		int nOffsetX=offsetX;
		int nOffsetY=offsetY;
		for (int i = 0; i < 10; i++) {
			g.drawImage(images.get("wall_long"), (images.get("wall_long").getWidth() - 1) * i-nOffsetX ,-nOffsetY,null);
		}
		
		Iterator<Button> buttonItr = buttons.values().iterator();
		while (buttonItr.hasNext()) {
			Button tmp=buttonItr.next();
			tmp.setOffset(nOffsetX,nOffsetY);
			tmp.draw(g);
		}
		
		drawObjects(g);
		hand.draw(g);
	}
}
