import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

public class ImageObject extends BreakedObject{
	Images images;
	int xsize,ysize;
	String filename;
	public ImageObject(String filename,ArgList lists, float x, float y,  int stageMinX,
			int stageMinY, int stageMaxX, int stageMaxY,Images images) {
		super(lists, x, y, 0, stageMinX, stageMinY, stageMaxX, stageMaxY);
		// TODO Auto-generated constructor stub
		this.filename=filename;
		this.images=images;
		images.loadImage(filename);
		this.ysize=images.get(filename).getHeight();
		this.xsize=images.get(filename).getWidth();
		this.size=Math.min(xsize, ysize);
	}
	public void draw(Graphics g) {
		double drawX = x - offsetX;
		double drawY = y - offsetY;
		Graphics2D g2 = (Graphics2D) g;
		if (status == stop) {
			AffineTransform at = new AffineTransform();
			at.setToRotation(Math.toRadians(rad), drawX, drawY);
			g2.setTransform(at);
			g2.drawImage(images.get(filename),(int) (drawX - xsize / 2), (int) (drawY - ysize / 2),null);
			at = new AffineTransform();
			g2.setTransform(at);
		} else if (status == hop) {
			AffineTransform at = new AffineTransform();
			at.setToRotation(Math.toRadians(rad), drawX, drawY);
			g2.setTransform(at);
			g2.drawImage(images.get(filename),(int) (drawX - xsize / 2), (int) (drawY - ysize / 2),null);
			g2.setColor(new Color(255,0,0,(int)(0.8*255*(1-(float)hp/(float)mhp))));
			g2.fillRect((int) (drawX - xsize / 2), (int) (drawY - ysize / 2), xsize, ysize);
			at = new AffineTransform();
			g2.setTransform(at);
		}
	}
}
