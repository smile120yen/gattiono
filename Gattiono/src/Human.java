import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

public class Human extends BreakedObject{

	Images images;
	int xsize,ysize,time=0;
	public Human(ArgList lists, float x, float y,  int stageMinX,
			int stageMinY, int stageMaxX, int stageMaxY,Images images) {
		super(lists, x, y, 0, stageMinX, stageMinY, stageMaxX, stageMaxY);
		// TODO Auto-generated constructor stub
		this.images=images;
		images.loadImage("human_stop");
		images.loadImage("human_hop1");
		images.loadImage("human_hop2");
		this.ysize=images.get("human_stop").getHeight();
		this.xsize=images.get("human_stop").getWidth();
		this.size=xsize;
		this.m=1;
	}
	public void updateAfter(){
		if(this.impact<-0.5){
			brks.add(new Anger(lists, (int)x, (int)y, 0, minX,minY, maxX, maxY));
		}
		hp=100000;
	}
	public void draw(Graphics g) {
		double drawX = x - offsetX;
		double drawY = y - offsetY;
		Graphics2D g2 = (Graphics2D) g;
		if (status == stop) {
			AffineTransform at = new AffineTransform();
			at.setToRotation(Math.toRadians(rad), drawX, drawY);
			g2.setTransform(at);
			g2.drawImage(images.get("human_stop"),(int) (drawX - xsize / 2), (int) (drawY - ysize / 2),null);
			at = new AffineTransform();
			g2.setTransform(at);
		} else if (status == hop) {
			AffineTransform at = new AffineTransform();
			at.setToRotation(Math.toRadians(rad), drawX, drawY);
			g2.setTransform(at);
			time++;
			if(time<75){
				g2.drawImage(images.get("human_hop1"),(int) (drawX - xsize / 2), (int) (drawY - ysize / 2),null);
			}else if(time<=150){
				g2.drawImage(images.get("human_hop2"),(int) (drawX - xsize / 2), (int) (drawY - ysize / 2),null);
				if(time==150){
					time=0;
				}
			}
			at = new AffineTransform();
			g2.setTransform(at);
		}
	}
	public void hitStage(){
		if (x < minX)

		{
			x = minX;
			hp -= this.m * vSize();
			vx *= -0.5;
		}
		if (x > maxX)

		{
			x = maxX;
			hp -= this.m * vSize();
			vx *= -0.5;
		}
		if (y < minY)

		{
			y = minY;
			hp -= this.m * vSize();
			vy *= -0.5;
		}
		if (y > maxY-100)

		{
			y = maxY-100;
			vy *= -0.5;
			vx *= 1;
		}
	}
}
