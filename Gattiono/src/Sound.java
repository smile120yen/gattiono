import java.io.File;
import java.io.IOException;
import javax.sound.sampled.*;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class Sound implements LineListener {
	Clip clip = null;
	AudioInputStream audioInputStream;
	boolean running=false,once=false;

	public Sound(String filename) {
		try {
			File soundFile = new File("sound/"+filename);
			audioInputStream = AudioSystem.getAudioInputStream(soundFile);
			AudioFormat audioFormat = audioInputStream.getFormat();
			DataLine.Info info = new DataLine.Info(Clip.class, audioFormat);
			clip = (Clip) AudioSystem.getLine(info);
			clip.open(audioInputStream);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		
		clip.addLineListener(this);
		clip.start();
		running=true;
	}
	public void setOnce(){
		this.stop();
		once=true;
	}
	public void start(){
		if (clip != null) {
			clip.setFramePosition(0);
			clip.start();
			running=true;
		}
	}
	public void stop() {
		if (clip != null) {
			clip.stop();
			running=false;
		}
	}

	public boolean isPlaying() {
		return running;
	}
	

	@Override
	public void update(LineEvent event) {
		// TODO 自動生成されたメソッド・スタブ
		if (event.getType() == LineEvent.Type.STOP) {
			clip.stop();
			clip.setFramePosition(0);
			running = false;
			clip.removeLineListener(this);
		}
	}

}
